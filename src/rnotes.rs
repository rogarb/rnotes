//! This module contains the definition of the Rnotes struct, which wraps all the crate's
//! functionality.
mod config;
mod git;
mod run;
mod tree;
mod utils;

use clap::{Parser, Subcommand};
use std::path::PathBuf;

#[cfg(feature = "git")]
use git2::Repository;

pub struct Rnotes {
    config: config::Config,
    args: Args,
    #[cfg(feature = "git")]
    repo: Repository,
}

impl Rnotes {
    /// Default path to config file
    const CONFIGFILE: &'static str = ".config/rnotes";
    /// Default filename for the backup file
    const BACKUPFILE: &'static str = "notesdir";
}

#[derive(Debug, Subcommand, Clone)]
pub enum Command {
    /// Create a new note
    #[command(alias = "add")]
    Create {
        /// Note name
        note: PathBuf,
    },
    /// List notes
    Ls { path: Option<PathBuf> },
    /// Edit a note with $EDITOR
    Edit {
        /// Note name
        note: PathBuf,
    },
    /// Remove a note
    Rm {
        /// Note name
        note: PathBuf,
    },
    /// Move a note
    Mv {
        /// Old note name
        from: PathBuf,
        /// New note name
        to: PathBuf,
    },
    /// Display a note on the screen with proper formatting
    #[command(alias = "view", alias = "print")]
    Show {
        /// Note name
        note: PathBuf,
    },
    /// Dump the raw content of the note on the screen
    #[command(alias = "cat")]
    Dump {
        /// Note name
        note: PathBuf,
    },
    /// Append standard input to the end of a note
    Append {
        /// Note name
        note: PathBuf,
    },
    /// Grep in notes
    Grep {
        /// Case insensitive search
        #[arg(short, long)]
        ignore_case: bool,
        /// Print non matching lines
        #[arg(short = 'v', long)]
        invert: bool,
        /// Print only filenames containing matching lines
        #[arg(short, long)]
        quiet: bool,
        /// Regular expression to match
        regex: String,
        /// Optional subdirectory
        subdir: Option<PathBuf>,
    },
    /// Find pattern in note names
    Find {
        /// Case insensitive search
        #[arg(short = 'i', long = "ignore-case")]
        ignore_case: bool,
        /// Print non matching lines
        #[arg(short = 'v', long = "invert-match")]
        invert: bool,
        /// Regular expression to match
        regex: String,
    },
    /// Generate completion
    Completion {
        /// Shell to generate completion for
        shell: clap_complete::Shell,
    },
    /// Prints the path to the notes folder
    Path,
    /// Backups the full NOTESDIR into an optionally compressed tarball                  
    Backup {
        /// Optionally compress the tarball using gzip                                   
        #[arg(short, long, value_enum)]
        compress: Option<Cmp>,
        /// Optional file name for the tarball (default will be notesdir-[TIMESTAMP].tar)
        filename: Option<PathBuf>,
    },
}

#[derive(Debug, Parser, Clone)]
#[command(about = "A CLI notes manager.", version, author)]
pub struct Args {
    #[command(subcommand)]
    pub command: Command,
    /// Specify the path to the cnofig file
    #[arg(short, long)]
    pub configfile: Option<PathBuf>,
}

/// Lists the available compression options for backup
#[derive(clap::ValueEnum, Clone, Debug)]
pub enum Cmp {
    /// Use gzip compression
    Gzip,
    /// Use bzip2 compression
    Bzip2,
}

impl Rnotes {
    pub fn init() -> anyhow::Result<Rnotes> {
        // parse environment to get the value of $USER
        let mut configfile = match std::env::var_os("HOME") {
            None => None,
            Some(path) => {
                let mut configfile = PathBuf::from(path);
                configfile.push(Self::CONFIGFILE);
                Some(configfile)
            }
        };

        let args = Args::parse();
        if args.configfile.is_some() {
            configfile = args.configfile.clone();
        }

        // populate the configuration from the configfile using config::Config
        // lib
        let config = config::Config::new(configfile)?;
        //
        Rnotes::validate_notesdir(config.notesdir())?;
        #[cfg(feature = "git")]
        let notesdir = &config.notesdir().clone();
        let ret = Rnotes {
            config,
            args,
            #[cfg(feature = "git")]
            repo: Rnotes::repo_init(notesdir)?,
        };
        #[cfg(feature = "git")]
        let mut ret = ret;
        #[cfg(feature = "git")]
        if ret.repo.head().is_err() {
            ret.commit("Initial commit")?;
        }
        Ok(ret)
    }
}
