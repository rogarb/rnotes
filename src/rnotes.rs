//! This module contains the definition of the Rnotes struct, which wraps all the crate's
//! functionality.
mod config;
mod git;
mod run;
mod tree;
mod utils;

use clap::Parser;
use std::path::PathBuf;

use crate::error::Error;

#[cfg(feature = "git")]
use git2::Repository;

pub struct Rnotes {
    config: config::Config,
    args: Args,
    #[cfg(feature = "git")]
    repo: Repository,
}

impl Rnotes {
    /// Default path to config file
    const CONFIGFILE: &'static str = ".config/rnotes";
    /// Default filename for the backup file
    const BACKUPFILE: &'static str = "notesdir";
}

#[derive(Debug, Parser, Clone)]
#[clap(about = "A CLI notes manager.")]
pub enum Command {
    /// Alias for create
    Add {
        /// Note name
        #[clap(parse(from_os_str))]
        note: PathBuf,
    },
    /// Create a new note
    Create {
        /// Note name
        #[clap(parse(from_os_str))]
        note: PathBuf,
    },
    /// List notes
    Ls {
        #[clap(parse(from_os_str))]
        path: Option<PathBuf>,
    },
    /// Edit a note with $EDITOR
    Edit {
        /// Note name
        #[clap(parse(from_os_str))]
        note: PathBuf,
    },
    /// Remove a note
    Rm {
        /// Note name
        #[clap(parse(from_os_str))]
        note: PathBuf,
    },
    /// Move a note
    Mv {
        /// Old note name
        #[clap(parse(from_os_str))]
        from: PathBuf,
        /// New note name
        #[clap(parse(from_os_str))]
        to: PathBuf,
    },
    /// Display a note on the screen with proper formatting
    Show {
        /// Note name
        #[clap(parse(from_os_str))]
        note: PathBuf,
    },
    /// Alias for show
    View {
        /// Note name
        #[clap(parse(from_os_str))]
        note: PathBuf,
    },
    /// Alias for show
    Print {
        /// Note name
        #[clap(parse(from_os_str))]
        note: PathBuf,
    },
    /// Alias for dump
    Cat {
        /// Note name
        #[clap(parse(from_os_str))]
        note: PathBuf,
    },
    /// Dump the raw content of the note on the screen
    Dump {
        /// Note name
        #[clap(parse(from_os_str))]
        note: PathBuf,
    },
    /// Append standard input to the end of a note
    Append {
        /// Note name
        #[clap(parse(from_os_str))]
        note: PathBuf,
    },
    /// Grep in notes
    Grep {
        /// Case insensitive search
        #[clap(short = 'i', long = "ignore-case")]
        ignore_case: bool,
        /// Print non matching lines
        #[clap(short = 'v', long = "invert-match")]
        invert: bool,
        /// Print only filenames containing matching lines
        #[clap(short, long)]
        quiet: bool,
        /// Regular expression to match
        regex: String,
        /// Optional subdirectory
        #[clap(parse(from_os_str))]
        subdir: Option<PathBuf>,
    },
    /// Find pattern in note names
    Find {
        /// Case insensitive search
        #[clap(short = 'i', long = "ignore-case")]
        ignore_case: bool,
        /// Print non matching lines
        #[clap(short = 'v', long = "invert-match")]
        invert: bool,
        /// Regular expression to match
        regex: String,
    },
    /// Generate completion
    Completion {
        /// Shell to generate completion for
        shell: clap_complete::Shell,
    },
    /// Prints the path to the notes folder
    Path,
    /// Backups the full NOTESDIR into an optionally compressed tarball                  
    Backup {
        /// Optionally compress the tarball using gzip                                   
        #[clap(short, long, value_enum)]
        compress: Option<Cmp>,
        /// Optional file name for the tarball (default will be notesdir-[TIMESTAMP].tar)
        #[clap(parse(from_os_str))]
        filename: Option<PathBuf>,
    },
}

#[derive(Debug, Parser, Clone)]
pub struct Args {
    #[clap(subcommand)]
    pub command: Command,
    /// Specify the path to the cnofig file
    #[clap(short, long, parse(from_os_str))]
    pub configfile: Option<PathBuf>,
}

/// Lists the available compression options for backup
#[derive(clap::ValueEnum, Clone, Debug)]
pub enum Cmp {
    /// Use gzip compression
    Gzip,
    /// Use bzip2 compression
    Bzip2,
}

impl Rnotes {
    pub fn init() -> Result<Rnotes, Error> {
        // parse environment to get the value of $USER
        let mut configfile = match std::env::var_os("HOME") {
            None => None,
            Some(path) => {
                let mut configfile = PathBuf::from(path);
                configfile.push(Self::CONFIGFILE);
                Some(configfile)
            }
        };

        let args = Args::parse();
        if args.configfile.is_some() {
            configfile = args.configfile.clone();
        }

        // populate the configuration from the configfile using config::Config
        // lib
        let config = config::Config::new(configfile);
        //
        Rnotes::validate_notesdir(config.notesdir())?;
        #[cfg(feature = "git")]
        let notesdir = &config.notesdir().clone();
        let ret = Rnotes {
            config,
            args,
            #[cfg(feature = "git")]
            repo: Rnotes::repo_init(notesdir)?,
        };
        #[cfg(feature = "git")]
        let mut ret = ret;
        #[cfg(feature = "git")]
        if ret.repo.head().is_err() {
            ret.commit("Initial commit")?;
        }
        Ok(ret)
    }
}
