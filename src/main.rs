mod rnotes;

use rnotes::Rnotes;

fn main() -> anyhow::Result<()> {
    Rnotes::init()?.run()
}
