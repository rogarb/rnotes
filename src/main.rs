mod error;
mod rnotes;

use error::Error;
use rnotes::Rnotes;

fn main() -> Result<(), Error> {
    Rnotes::init()?.run()
}
