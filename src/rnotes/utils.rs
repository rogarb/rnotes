use super::Rnotes;
use std::{
    fs,
    path::{Path, PathBuf},
    vec::Vec,
};

use crate::error::Error;

pub fn count_lines(path: &Path) -> Result<usize, Error> {
    let file = fs::read(&path)?;
    let mut count: usize = 0;
    for byte in file {
        if byte == b'\n' {
            count += 1;
        }
    }
    Ok(count)
}

pub fn is_hidden(path: &Path) -> bool {
    let path = path.to_path_buf();
    let filename = path.file_name().unwrap().to_str().unwrap();
    if filename.starts_with('.') {
        return true;
    }
    false
}

pub fn rec_iter(subdir: &Path, vec: &mut Vec<PathBuf>) {
    for entry in std::fs::read_dir(subdir).unwrap() {
        let entry = entry.unwrap().path();
        // ignore hidden files
        if !is_hidden(&entry) {
            if entry.is_file() {
                vec.push(entry);
            } else if entry.read_dir().unwrap().last().is_some() {
                // entry is a non empty dir
                rec_iter(&entry, vec);
            }
        }
    }
}

impl Rnotes {
    pub fn validate_notesdir(notesdir: &Path) -> Result<(), Error> {
        if !notesdir.is_dir() {
            let msg = format!("{:?} is not a directory", notesdir);
            return Err(Error::new(&msg));
        }
        Ok(())
    }

    // validates the existence of a subdir in the notes folder
    pub fn validate_subdir(&self, subdir: &Path) -> Result<(), Error> {
        self.validate_item_in_path(subdir)?;

        let subdir = self.build_fullpath(subdir);
        if !subdir.is_dir() {
            let msg = format!("{:?} is not a directory", subdir);
            return Err(Error::new(&msg));
        }
        Ok(())
    }

    // validates the existence of an item in the notes folder
    pub fn validate_item_in_path(&self, item: &Path) -> Result<(), Error> {
        let item = self.build_fullpath(item);

        if !item.exists() {
            let msg = format!("{:?} doesn't exist", item);
            return Err(Error::new(&msg));
        }
        Ok(())
    }

    pub fn build_relpath(&self, path: &Path) -> PathBuf {
        let path = PathBuf::from(path);
        path.strip_prefix(self.config.notesdir())
            .expect("BUG: fullpath should contain notedir")
            .to_path_buf()
    }

    pub fn build_fullpath(&self, path: &Path) -> PathBuf {
        self.config.notesdir().join(path)
    }
}
