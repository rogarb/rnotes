use super::Rnotes;
use anyhow::anyhow;
use std::{
    fs,
    path::{Path, PathBuf},
    vec::Vec,
};

pub fn count_lines(path: &Path) -> anyhow::Result<usize> {
    let file = fs::read(path)?;
    let mut count: usize = 0;
    for byte in file {
        if byte == b'\n' {
            count += 1;
        }
    }
    Ok(count)
}

pub fn is_hidden(path: &Path) -> bool {
    path.file_name()
        .expect("BUG: unable to find filename for {path}")
        .to_str()
        .expect("BUG: should be a valid UTF-8 path (\"{path}\"")
        .starts_with('.')
}

pub fn rec_iter(subdir: &Path, vec: &mut Vec<PathBuf>) -> Result<(), std::io::Error> {
    for entry in std::fs::read_dir(subdir)? {
        let entry = entry?.path();
        // ignore hidden files
        if !is_hidden(&entry) {
            if entry.is_file() {
                vec.push(entry);
            } else if entry.read_dir()?.last().is_some() {
                // entry is a non empty dir
                rec_iter(&entry, vec)?;
            }
        }
    }
    Ok(())
}

impl Rnotes {
    pub fn validate_notesdir(notesdir: &Path) -> anyhow::Result<()> {
        if !notesdir.is_dir() {
            Err(anyhow!("{:?} is not a directory", notesdir))
        } else {
            Ok(())
        }
    }

    // validates the existence of a subdir in the notes folder
    pub fn validate_subdir(&self, subdir: &Path) -> anyhow::Result<()> {
        self.validate_item_in_path(subdir)?;

        let subdir = self.build_fullpath(subdir);
        if !subdir.is_dir() {
            Err(anyhow!("{:?} is not a directory", subdir))
        } else {
            Ok(())
        }
    }

    // validates the existence of an item in the notes folder
    pub fn validate_item_in_path(&self, item: &Path) -> anyhow::Result<()> {
        let item = self.build_fullpath(item);

        if !item.exists() {
            Err(anyhow!("{:?} doesn't exist", item))
        } else {
            Ok(())
        }
    }

    pub fn build_relpath(&self, path: &Path) -> PathBuf {
        let path = PathBuf::from(path);
        path.strip_prefix(self.config.notesdir())
            .expect("BUG: fullpath should contain notedir")
            .to_path_buf()
    }

    pub fn build_fullpath(&self, path: &Path) -> PathBuf {
        self.config.notesdir().join(path)
    }
}
