//! This module contains the bits related to the run() method.
//!
use super::tree::Tree;
use super::utils;
use super::Args;
use super::Cmp;
use super::Command::*;
use super::Rnotes;
use anyhow::{anyhow, bail};
use bat::PrettyPrinter;
use chrono::Local;
use clap::CommandFactory;
use regex::Regex;
use std::{
    env,
    path::{Path, PathBuf},
    process::Command,
    vec::Vec,
};

impl Rnotes {
    /// Interface to run the program
    pub fn run(&mut self) -> anyhow::Result<()> {
        let command = self.args.to_owned().command;
        match command {
            Ls { path } => self.ls(path),
            Show { note } => self.show(note),
            Create { note } => self.create(note),
            Edit { note } => self.edit(note),
            Rm { note } => self.rm(note),
            Dump { note } => self.dump(note),
            Append { note } => self.append(note),
            Grep {
                ignore_case,
                invert,
                quiet,
                regex,
                subdir,
            } => self.grep((ignore_case, invert, quiet), regex, subdir),
            Find {
                ignore_case,
                invert,
                regex,
            } => self.find((ignore_case, invert), regex),
            Mv { from, to } => self.mv(from, to),
            Completion { shell } => self.gen_completion(shell),
            Path => self.path(),
            Backup { compress, filename } => self.backup(compress, filename),
        }
    }

    fn create_if_not_exists(&mut self, note: &Path) -> anyhow::Result<()> {
        let note = self.build_fullpath(note);
        if !note.exists() {
            println!("File {:?} doesn't exist, creating...", &note);
            self.create_helper(note.strip_prefix(&self.config.notesdir())?)?;
        }
        Ok(())
    }

    fn create_helper(&mut self, note: &Path) -> anyhow::Result<()> {
        let note = self.build_fullpath(note);
        if note.exists() {
            bail!("File {:?} already exists", note);
        }
        // ensure parent exists
        let parent = note
            .parent()
            .ok_or(anyhow!("BUG: \"{}\" has no parent", note.display()))?;
        if !parent.exists() {
            std::fs::create_dir_all(parent)?;
        }

        let mut header = String::from("Created on ");
        header.push_str(Local::now().to_rfc2822().as_str());
        header.push_str("\n------------------------------------------\n");
        std::fs::write(&note, header)?;
        Ok(())
    }

    fn create(&mut self, note: PathBuf) -> anyhow::Result<()> {
        self.create_helper(&note)?;
        self.edit(note)
    }

    fn ls(&self, item: Option<PathBuf>) -> anyhow::Result<()> {
        let dir = self.config.notesdir().clone();
        if let Some(item) = item.clone() {
            self.validate_item_in_path(&item)?;
        }
        let mut tree = Tree::init(&dir, item)?;
        tree.display();
        Ok(())
    }

    fn edit(&mut self, note: PathBuf) -> anyhow::Result<()> {
        self.create_if_not_exists(&note)?;
        let editor = match env::var_os("EDITOR") {
            Some(val) => val,
            None => bail!("EDITOR is not set"),
        };
        let mut command = Command::new(editor.clone());
        let editor = PathBuf::from(editor.as_os_str());
        let vim = Regex::new("(vi.*|.*vi)")?;
        if vim.is_match(
            &editor
                .file_name()
                .ok_or(anyhow!("BUG: EDITOR path should have a file name"))?
                .to_string_lossy(),
        ) {
            command.args(["-c", "set ft=asciidoc"]);
        }
        let note = self.build_fullpath(&note);
        command.arg(note);
        let mut child = command.spawn()?;
        child.wait()?;
        #[cfg(feature = "git")]
        self.commit("Edition")?;
        Ok(())
    }

    fn mv(&mut self, from: PathBuf, to: PathBuf) -> anyhow::Result<()> {
        let mut to = self.build_fullpath(&to);
        let from = self.build_fullpath(&from);

        if to.exists() && (!to.is_dir()) {
            Err(anyhow!("{}: path already exists", to.to_string_lossy()))
        } else if !from.exists() {
            Err(anyhow!("{}: path doesn't exist", from.to_string_lossy()))
        } else {
            if to.is_dir() && (!from.is_dir()) {
                to.push(from.file_name().ok_or(anyhow!(
                    "BUG: \"from\" path ({}) should have a file name",
                    from.display()
                ))?);
            }
            if let Some(path) = to.parent() {
                if !path.exists() {
                    std::fs::create_dir(path)?;
                }
            }
            std::fs::rename(from, to)?;
            #[cfg(feature = "git")]
            self.commit("Rename")?;
            Ok(())
        }
    }

    fn rm(&mut self, note: PathBuf) -> anyhow::Result<()> {
        let note = self.build_fullpath(&note);
        std::fs::remove_file(note)?;
        #[cfg(feature = "git")]
        self.commit("Suppression")?;
        Ok(())
    }

    fn show(&self, note: PathBuf) -> anyhow::Result<()> {
        let note = self.build_fullpath(&note);
        PrettyPrinter::new()
            .header(true)
            .grid(true)
            .line_numbers(true)
            .input_file(note)
            .print()
            .expect("BUG: unable to print note on screen");
        Ok(())
    }

    fn dump(&self, note: PathBuf) -> anyhow::Result<()> {
        let note = self.build_fullpath(&note);
        println!("{}", std::fs::read_to_string(note)?);
        Ok(())
    }

    fn append(&mut self, note: PathBuf) -> anyhow::Result<()> {
        self.create_if_not_exists(&note)?;
        let mut note = std::fs::OpenOptions::new()
            .append(true)
            .create(true)
            .open(&note)?;
        std::io::copy(&mut std::io::stdin(), &mut note)?;
        #[cfg(feature = "git")]
        self.commit("Append")?;
        Ok(())
    }

    fn grep(
        &self,
        flags: (bool, bool, bool),
        regex: String,
        subdir: Option<PathBuf>,
    ) -> anyhow::Result<()> {
        let (ign_case, invert_match, quiet) = flags;
        let subdir = match subdir {
            Some(path) => {
                self.validate_subdir(&path)?;
                self.build_fullpath(&path)
            }
            None => self.config.notesdir().to_path_buf(),
        };
        let mut files: Vec<PathBuf> = Vec::new();
        utils::rec_iter(&subdir, &mut files)?;

        let mut matcher = grep::regex::RegexMatcherBuilder::new();
        matcher.case_insensitive(ign_case);
        let matcher = matcher.build(&regex)?;

        for file in files {
            let mut searcher = grep::searcher::SearcherBuilder::new();
            searcher.invert_match(invert_match);
            let mut searcher = searcher.build();
            let mut matches: Vec<(u64, String)> = Vec::new();
            searcher.search_path(
                matcher.clone(),
                &file,
                grep::searcher::sinks::UTF8(|num, line| {
                    matches.push((num, line.to_string()));
                    Ok(true)
                }),
            )?;
            if !quiet {
                if !matches.is_empty() {
                    let file = self.build_relpath(&file);
                    println!("{}:", file.display());
                    for (num, line) in matches {
                        println!("{}: {}", num, line.trim_end());
                    }
                    println!();
                }
            } else if invert_match {
                // if in quiet mode, only output the filenames which
                // don't contain the pattern at all
                let lines = utils::count_lines(&file)?;
                let file = self.build_relpath(&file);
                if matches.len() == lines {
                    println!("{}", file.display());
                }
            } else {
                let file = self.build_relpath(&file);
                if !matches.is_empty() {
                    println!("{}", file.display());
                }
            }
        }
        Ok(())
    }

    fn find(&self, flags: (bool, bool), regex: String) -> anyhow::Result<()> {
        let (ign_case, invert_match) = flags;
        let mut files: Vec<PathBuf> = Vec::new();
        utils::rec_iter(self.config.notesdir(), &mut files)?;

        let mut matcher = grep::regex::RegexMatcherBuilder::new();
        matcher.case_insensitive(ign_case);
        let matcher = matcher.build(&regex)?;

        for filename in files {
            let mut searcher = grep::searcher::SearcherBuilder::new();
            searcher.invert_match(invert_match);
            let mut searcher = searcher.build();
            let mut matches: Vec<(u64, String)> = Vec::new();
            let filename = self.build_relpath(&filename);
            let filename = filename.to_string_lossy();
            searcher.search_slice(
                matcher.clone(),
                filename.as_bytes(),
                grep::searcher::sinks::UTF8(|num, line| {
                    matches.push((num, line.to_string()));
                    Ok(true)
                }),
            )?;
            for (_num, line) in matches {
                println!("{}", line.trim_end());
            }
        }
        Ok(())
    }

    fn path(&self) -> anyhow::Result<()> {
        match self.config.notesdir().to_str() {
            Some(s) => {
                println!("{}", s);
                Ok(())
            }
            None => Err(anyhow!("No path set for NOTESDIR")),
        }
    }
    // Generates the completion
    fn gen_completion(&self, shell: clap_complete::Shell) -> anyhow::Result<()> {
        match shell {
            // fix the generated output for Zsh case in order to get direct completion
            // from NOTESDIR
            clap_complete::Shell::Zsh => {
                let mut s = Vec::new();
                clap_complete::generate(
                    shell,
                    &mut Args::command(),
                    Args::command().get_name(),
                    &mut s,
                );
                let s = match String::from_utf8(s) {
                    Ok(s) => s,
                    _ => bail!("Unable to convert completion content"),
                };
                // adds a variable to store the NOTESDIR path
                let s = s.replace(
                    "local ret=1",
                    format!(
                        "local notespath=$({} path)\n    local ret=1",
                        Args::command().get_name()
                    )
                    .as_str(),
                );
                // complete the filenames with the names contained in $notespath
                let s = s.replace("_files", "_files -W $notespath");
                println!("{}", s);
            }
            // use default method for other shells
            _ => {
                clap_complete::generate(
                    shell,
                    &mut Args::command(),
                    Args::command().get_name(),
                    &mut std::io::stdout(),
                );
            }
        }
        Ok(())
    }

    fn backup(&self, compress: Option<Cmp>, filename: Option<PathBuf>) -> anyhow::Result<()> {
        // get the basefile name
        let mut filename = if let Some(filename) = filename {
            filename
        } else {
            PathBuf::from(Rnotes::BACKUPFILE)
        };
        // automatically add the extension based on options
        if let Some(compress) = compress.clone() {
            match compress {
                Cmp::Gzip => {
                    filename.set_extension("tar.gz");
                }
                Cmp::Bzip2 => {
                    filename.set_extension("tar.bz2");
                }
            }
        } else {
            filename.set_extension("tar");
        }

        if filename.exists() {
            bail!("File {} already exists, aborting", filename.display());
        }

        let mut command = std::process::Command::new("tar");
        command
            .arg("-c")
            .arg("-f")
            .arg(format!("{}", filename.display()))
            .arg("--transform")
            .arg(format!(
                "s|{}|NOTESDIR|",
                self.config.notesdir().strip_prefix("/")?.display()
            ));
        if let Some(compress) = compress {
            command.arg(match compress {
                Cmp::Gzip => "-z",
                Cmp::Bzip2 => "-J",
            });
        }
        command.arg(format!("{}", self.config.notesdir().display()).as_str());

        if command.status()?.success() {
            eprintln!("Backup file successfully writted to {}", filename.display());
            Ok(())
        } else {
            Err(anyhow!("Unable to execute tar command"))
        }
    }
}
