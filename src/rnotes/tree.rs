use std::{
    collections::BTreeMap,
    fs::read_dir,
    path::{Path, PathBuf},
};

use ansi_term::{Color, Style};

use crate::error::Error;
use crate::rnotes::utils;

pub struct Tree {
    // path, (depth, is it the last item in the dir?)
    tree: BTreeMap<PathBuf, (u8, bool)>,
    dir_style: Style,
    note_style: Style,
    tree_style: Style,
}

impl Tree {
    pub fn init(path: &Path, item: Option<PathBuf>) -> Result<Self, Error> {
        let mut btree = BTreeMap::new();
        let path = path.to_path_buf();
        if !path.exists() {
            let msg = format!("{:?} doesn't exist", path);
            return Err(Error::new(&msg));
        }

        // extract the parentdir of the path
        let path = match item {
            Some(item) => {
                let mut fullpath = path;
                fullpath.push(item.clone());
                if fullpath.is_dir() {
                    fullpath
                } else {
                    match fullpath.parent() {
                        Some(path) => {
                            btree.insert(path.to_path_buf(), (0u8, false));
                            btree.insert(item, (1u8, true));
                            path.to_path_buf()
                        }
                        _ => {
                            let msg = format!("Path \"{:?}\" has no parent", fullpath);
                            return Err(Error::new(&msg));
                        }
                    }
                }
            }
            None => path,
        };

        let mut tree = Tree {
            tree: btree,
            dir_style: Style::new().bold().fg(Color::Blue),
            note_style: Style::new().fg(Color::White),
            tree_style: Style::new().fg(Color::RGB(0x66, 0x66, 0x66)),
        };
        if tree.tree.is_empty() {
            tree.recurse_add(path, 0u8)?;
            tree.compute_last();
        }
        Ok(tree)
    }

    fn recurse_add(&mut self, path: PathBuf, depth: u8) -> Result<(), Error> {
        self.tree.insert(path.clone(), (depth, false));
        let depth = depth + 1;
        let last_entry = path.read_dir()?.last().unwrap()?.path();
        for entry in read_dir(&path)? {
            let entry = entry?.path();
            if entry.to_str().unwrap() == last_entry.to_str().unwrap() {}
            if !utils::is_hidden(&entry) {
                if entry.is_dir() && entry.read_dir()?.last().is_some() {
                    self.recurse_add(entry, depth)?;
                } else {
                    self.tree.insert(entry.clone(), (depth, false));
                }
            }
        }
        Ok(())
    }

    // compute the "last" field of the tuple in the tree
    // assume all the bool in the tree are false
    fn compute_last(&mut self) {
        let tmp_tree = self.tree.clone();
        let mut last_path = PathBuf::new();
        let mut last_depth = 0;
        let mut max_depth = 0;

        for (path, (depth, last)) in tmp_tree.iter() {
            let depth = *depth;
            let last = *last;
            if last {
                panic!("BUG: last should be set to false at creation time");
            }
            // as the paths are sorted, when depth decrements, it means that
            // we reached the last item of the folder
            if last_depth > depth {
                let prev_entry = self.tree.get_mut(&last_path);
                prev_entry.expect("BUG: no value for existing key").1 = true;
            }
            last_path = path.clone();
            last_depth = depth;
            max_depth = if depth > max_depth { depth } else { max_depth };
        }
        // set the last parameter for each depth between 1 and max_depth
        while max_depth > 0 {
            for (path, (depth, _last)) in tmp_tree.iter() {
                let depth = *depth;
                if depth == max_depth {
                    last_path = path.clone()
                }
            }
            let last_key = self.tree.get_mut(&last_path);
            last_key.expect("BUG: no value for existing key").1 = true;
            max_depth -= 1;
        }
    }

    pub fn display(&mut self) {
        // taken from exa source code
        let edge = "├── "; // Rightmost column, *not* the last in the directory.
        let corner = "└── "; // Rightmost column, and the last in the directory.
        let line = "│   "; // Not the rightmost column, and the directory has not finished yet.
        let blank = "    "; // Not the rightmost column, and the directory *has* finished.
        let mut prev_last = true;
        let mut prev_depth = 0;
        let mut tree_lines = String::new();
        for (path, (depth, last)) in self.tree.iter() {
            let path = &*path;
            let depth = *depth;
            let last = *last;
            if depth == 0 {
                println!("{}:", path.to_str().unwrap());
            } else {
                if prev_last {
                    // we strip the occurences of "  " and "│ " as many times as
                    // steps between depth and prev_depth
                    for _i in depth..prev_depth {
                        // we first try to strip "  ", if it returns Some, it
                        // was there, if not we strip "│ "
                        tree_lines = match tree_lines.strip_suffix(blank) {
                            Some(str) => String::from(str),
                            None => match tree_lines.strip_suffix(line) {
                                Some(str) => String::from(str),
                                None => panic!("BUG: neither \"  \" nor \"│ \" is a suffix"),
                            },
                        }
                    }
                    // if we go down in the directory tree, depth increments of only 1
                }
                if prev_depth < depth && depth > 1 {
                    assert_eq!(prev_depth + 1, depth);
                    tree_lines.push_str(if prev_depth + 1 != depth || prev_last {
                        blank
                    } else {
                        line
                    });
                }
                let trunc_len = tree_lines.len();
                tree_lines.push_str(if last { corner } else { edge });
                let filename = path.file_name().unwrap().to_str().unwrap();
                let filename = if path.is_dir() {
                    self.dir_style.paint(filename)
                } else {
                    self.note_style.paint(filename)
                };
                println!("{}{}", self.tree_style.paint(&tree_lines), filename,);
                prev_last = last;
                prev_depth = depth;
                tree_lines.truncate(trunc_len);
            }
        }
    }
}
