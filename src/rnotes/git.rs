//! This module contains git related bits
//!
use super::Rnotes;
use crate::error::Error;
#[cfg(feature = "git")]
use git2::Repository;
use std::{
    io::Write,
    path::{Path, PathBuf},
};

impl Rnotes {
    #[cfg(feature = "git")]
    pub fn repo_init(path: &Path) -> Result<Repository, Error> {
        match Repository::open(path) {
            Ok(repo) => Ok(repo),
            Err(_e) => {
                let repo = Repository::init(path)?;
                let mut gitignore = PathBuf::from(path);
                gitignore.push("gitignore");
                let mut gitignore = std::fs::File::create(gitignore)?;
                std::fs::File::write(&mut gitignore, b".*.swp\n")?;
                Ok(repo)
            }
        }
    }

    #[cfg(feature = "git")]
    pub fn commit(&mut self, msg: &str) -> Result<(), Error> {
        let mut index = self.repo.index()?;
        index.add_all(["*"].iter(), git2::IndexAddOption::DEFAULT, None)?;
        index.write()?;
        let tree_id = index.write_tree()?;
        let tree = self.repo.find_tree(tree_id)?;
        let sig = git2::Signature::now("rnotes", "none")?;
        match self.repo.head() {
            Ok(commit) => {
                let parent = commit.peel_to_commit()?;
                self.repo
                    .commit(Some("HEAD"), &sig, &sig, msg, &tree, &[&parent])?;
            }
            Err(_e) => {
                self.repo
                    .commit(Some("HEAD"), &sig, &sig, msg, &tree, &[])?;
            }
        };
        Ok(())
    }
}
