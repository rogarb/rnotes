use std::{default::Default, path::PathBuf, process::exit};

use tini::Ini;

#[cfg(feature = "git")]
use core::str::FromStr;

#[derive(Debug)]
pub struct Config {
    notesdir: std::path::PathBuf,
    #[cfg(feature = "git")]
    git: bool,
}

impl Default for Config {
    fn default() -> Self {
        Config {
            notesdir: PathBuf::from(".notes"),
            #[cfg(feature = "git")]
            git: false,
        }
    }
}

impl Config {
    // TODO: add error handling
    pub fn new(configfile: Option<std::path::PathBuf>) -> Config {
        let mut config = Config::default();
        // get values from config file if provided
        if let Some(cfgfile) = configfile {
            if let Ok(ini) = Ini::from_file(&cfgfile) {
                // assume there is only one section
                // TODO: throw an error if the config file has more than one section
                if let Some((_sec_name, section)) = ini.iter().last() {
                    for (key, value) in section.iter() {
                        match key.as_str() {
                            "notesdir" => config.notesdir = PathBuf::from(value),
                            #[cfg(feature = "git")]
                            "git" => config.git = FromStr::from_str(value).unwrap(),
                            _ => {
                                eprintln!("Unknown key: {:?}", key);
                                exit(1);
                            }
                        }
                    }
                }
            }
        }
        // get values from ENV and overwrite if necessary
        let notesdir: &str = "NOTESDIR";
        if let Some(env_notesdir) = std::env::var_os(notesdir) {
            config.notesdir = PathBuf::from(env_notesdir.to_str().unwrap());
        }

        // return config
        config
    }

    pub fn notesdir(&self) -> &PathBuf {
        &self.notesdir
    }
}
