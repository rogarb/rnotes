use std::fmt;

pub struct Error {
    message: String,
}

impl Error {
    pub fn new(msg: &str) -> Error {
        Error {
            message: String::from(msg),
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.message)
    }
}

impl fmt::Debug for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.message)
    }
}

#[cfg(feature = "git")]
impl From<git2::Error> for Error {
    fn from(err: git2::Error) -> Self {
        let msg = format!("libgit: {}", err.message());
        Error::new(&msg)
    }
}

impl From<std::io::Error> for Error {
    fn from(err: std::io::Error) -> Self {
        let msg = format!("IO: {}", err);
        Error::new(&msg)
    }
}

impl From<grep::regex::Error> for Error {
    fn from(err: grep::regex::Error) -> Self {
        let msg = format!("IO: {}", err);
        Error::new(&msg)
    }
}
