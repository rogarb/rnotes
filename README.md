rnotes: Rust version of notes-cli (https://gitlab.com/rogarb/cli-notes)


Installation
---

```cargo install --git https://gitlab.com/rogarb/rnotes.git```


Usage
---
See `rnotes help` for usage

